/******************************************************************************/
/*                                                                            */
/*                                                          FILE: measure.cpp */
/*                                                                            */
/*    Measures an analog value using an MCP3202 wired to a Raspberry PI       */
/*    =================================================================       */
/*                                                                            */
/*    The Microchip MCP3202 is a two channel 12bit AD converter. It was       */
/*    connected to the SPI bus of the Raspberry PI as shown at                */
/*    https://blog.heimetli.ch/raspberry-pi-mcp3202-12bit-ad-converter.html   */
/*                                                                            */
/*    This code was compiled with g++ and run under the Raspbian OS. It       */
/*    uses the spidev driver bundled with current Raspbian releases.          */
/*                                                                            */
/*    The program uses the device file spidev0.0 which belongs to root        */
/*    by default. For a first test run it like this: sudo ./measure           */
/*                                                                            */
/*    V0.01  07-SEP-2013   Te                                                 */
/*                                                                            */
/******************************************************************************/

#include <atomic>
#include <csignal>
#include <iostream>
#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <string.h>
#include <fcntl.h>
#include <sys/ioctl.h>
#include <curses.h>
#include <lo/lo.h>
#include <lo/lo_cpp.h>
#include <pigpio.h> 
#include "device.h"

namespace
{
  volatile std::sig_atomic_t gSignalStatus = 0;
}



void signal_handler(int signal)
{
   std::cout << "Ended with Signal: " << signal << std::endl;
   gSignalStatus = signal;
}

int main()
{

   mofa::DevicePtr device = std::make_shared<mofa::Device>( );
   if( ! device->openSpiDevice( "/dev/spidev0.0" ) )
   {
      fprintf( stderr, "can't open device\n" );
      return 1;
   }

   /*if( ! device->openSpiDevice( "/dev/spidev0.1" ) )
   {
      fprintf( stderr, "can't open device\n" );
      return 1;
   }*/

   if (gpioInitialise() < 0)
   {
      fprintf( stderr, "can't init gpio" );
      return -1;
   }

   SCREEN *screen = newterm(getenv("TERM"), stdout, stdin);
   if( !screen )
   {
      fprintf(stderr, "Error initialising ncurses.\n");
      return 4;
   }
   WINDOW *info = newwin( 20, 80, 0, 0 );
   wborder( info, 0, 0, 0, 0, 0, 0, 0, 0 );
   WINDOW *console = newwin( 60, 80, 21, 0 );
   wborder( console, 0, 0, 0, 0, 0, 0, 0, 0 );
   scrollok( console, true ); 
   wrefresh( info );
   wrefresh( console );
   device->setWindow( info );

   ///////////////////////////////////////////
   // TODO: call for each hardeware channel //
   ///////////////////////////////////////////
   device->addChannel( 0, 99 );

   std::signal(SIGINT, signal_handler);

   lo::ServerThread st(9000);

   if (!st.is_valid()) {
      fprintf( stderr, "can't init OSC-Server\n" );
      return 1;
   }

   st.set_callbacks([&st, info](){
      char str[80];
      sprintf( str, "Thread init: %p.\n",&st );
         mvwaddstr( info, 18, 2, str  );
         wrefresh( info );
      },
      [info](){
         mvwaddstr( info, 18, 2, "OSC Thread completed" );
         wrefresh( info );
      ;});

   char str[80];
   sprintf( str, "URL: %s", st.url().c_str() );
   mvwaddstr( info, 20, 0, str  );

   st.add_method("/strip/fader", "if",
               [=](const char *path, const char *types, lo_arg **argv, int argc)
               {
                  if( argc != 2 )
                     return 1;
                  //wprintw( console, "Channel %d - Fader: %f \n", argv[0]->i, argv[1]->f );
                  //wrefresh( console );
                  device->setFaderValue( argv[0]->i, argv[1]->f );
                  return 0;
               });

   st.add_method("/strip/solo", "if",
               [=](const char *path, const char *types, lo_arg **argv, int argc)
               {
                  if( argc != 2 )
                     return 1;
                  device->setSoloButtonState( argv[0]->i, argv[1]->f );
                  // wprintw( console, "Channel %d - Solo: %s \n", argv[0]->i, (argv[1]->f == 0 ? "Off" : "On") );
                  // wrefresh( console );
                  return 0;
               });

   st.add_method("/strip/mute", "if",
               [=](const char *path, const char *types, lo_arg **argv, int argc)
               {
                  if( argc != 2 )
                     return 1;
                  device->setMuteButtonState( argv[0]->i, argv[1]->f );
                  // wprintw( console, "Channel %d - Mute: %s \n", argv[0]->i, (argv[1]->f == 0 ? "Off" : "On") );
                  // wrefresh( console );
                  return 0;
               });

   st.add_method("/strip/select", "if",
               [=](const char *path, const char *types, lo_arg **argv, int argc)
               {
                  if( argc != 2 )
                     return 1;
                  // wprintw( console, "Channel %d - Select Bank %f\n", argv[0]->i, argv[0]->f );
                  // wrefresh( console );
                  return 0;
               });

   st.add_method(NULL, NULL,
               [=](const char *path, const char *types, lo_arg **argv, int argc)
               {
                  /*wprintw( console, "Path: %s\n", path );
                  for( int i = 0; i < argc; ++i ){
                     wprintw( console, "\t %02i %c: ", i,  types[i] );
                     switch( types[i]  )
                     {
                        case 'i': wprintw( console, "%d", argv[i]->i );
                        break;
                        case 'f': wprintw( console, "%f", argv[i]->f );
                        break;
                        //case 's': wprintw( console, "%s", argv[i]->s );
                        //break;
                        default:
                        break;
                     }
                     waddch( console, '\n' );
                  }
                  wrefresh( console );
                  wrefresh( info );*/
                  return 1;
               });

   st.start();
   device->setOscClientHost( "192.168.201.44" );
   device->setOscClientPort( "3819" );
   if( device->createOscClient() )
      device->setupOscClient();

   while( gSignalStatus == 0 )
   {
      if( !device->poll() )
      {
         wprintw( console, "Polling failed" );
         break;
      }
      usleep(10000);
   }
   sleep( 1 );
   st.stop();
   gpioTerminate();
   device->closeSpiDevice();
   delwin(info);
   delwin(console);
   endwin();
   delscreen(screen);
   return 0 ;
}

