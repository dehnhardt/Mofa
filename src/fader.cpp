#include "fader.h"

#include <algorithm>
#include <curses.h>
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/ioctl.h>
#include <unistd.h>
#include <pigpio.h> 

#include "channel.h"

namespace mofa
{

Fader::Fader( Channel *channel ) : m_pChannel( channel )
{

   m_sSpiADCTransferStruct = channel->getAdcSpiTransferData();
   m_sSpiGPIOTransferStruct = channel->getGpioSpiTransferData();


   if( !channel->getAdcTxBytes( 1, 0, m_cFaderTx ) )
      return;

   if( !channel->getAdcTxBytes( 1, 1, m_cTouchTx ) )
      return;

   gpioInitialise();
   gpioSetMode( 14, PI_OUTPUT );
   gpioSetMode( 15, PI_OUTPUT );
   gpioSetMode( 16, PI_OUTPUT );
   gpioWrite( 14, 0 );
   gpioWrite( 15, 0 );
   gpioWrite( 16, 0 );
   gpioPWM( 18, 0);
   gpioSetPWMrange( 18, 30 );
   gpioSetPWMfrequency( 18, 150);

}

void Fader::setInternalFaderValue( float oscDeviceValue )
{
   // only set the value if the fader is not manually moved
   //if( !m_bTouch )
      m_iFaderSetPointValue = getInternalFaderValue( oscDeviceValue );

}

int Fader::queryFaderValue( const int fileDescriptor )
{
	memset( m_cRx, 0, sizeof(m_cRx) );
	m_sSpiADCTransferStruct.tx_buf = (unsigned long)m_cFaderTx;
	m_sSpiADCTransferStruct.rx_buf = (unsigned long)m_cRx;

   m_iCurrentFaderValue = queryDevice( fileDescriptor );
   int diff = abs( static_cast<int>( m_iCurrentFaderValue - m_iLastFaderValue ) );
   if( diff > 10 || m_bStart )
   {
      sendDeviceFaderValue();
      if( m_pWindow )
      {
         char str[80];
         unsigned int mic = m_iCurrentFaderValue;
         sprintf( str, "I       %05d", mic ) ;
         mvwaddstr( m_pWindow, 7, 2, str  );
         wrefresh( m_pWindow );
      }
      m_iLastFaderValue = m_iCurrentFaderValue;
   }
   m_bStart = false;

   if( !m_bTouch )
   {
      int diff = m_iCurrentFaderValue - m_iFaderSetPointValue;

      unsigned int mic = m_iCurrentFaderValue;

      char str[80];
      sprintf( str, "A %05d %05d %05d", m_iFaderSetPointValue, mic, diff ) ;
      mvwaddstr( m_pWindow, 8, 2, str  );
      wrefresh( m_pWindow );
      int pw = abs(diff) > 150 ? 30 : abs(diff) > 40 ? 20 : 10;  
//      int pw = abs(diff) > 150 ? 30 : 20;  

      gpioPWM( 18, pw );

      if( diff < -30 )
      {
         gpioWrite( 14, 1 );
         gpioWrite( 15, 0 );
         mvwaddstr( m_pWindow, 8, 23, "1 ist kleiner soll "  );
      }
      else if( diff > 30 )
      {
         gpioWrite( 14, 0 );
         gpioWrite( 15, 1 );
         mvwaddstr( m_pWindow, 8, 23, "2 soll kleiner ist "  );
      } else
      {
         gpioWrite( 14, 0 );
         gpioWrite( 15, 0 );
         mvwaddstr( m_pWindow, 8, 23, "3 gleich           "  );
      }
   } 
   else
   {
      gpioWrite( 14, 0 );
      gpioWrite( 15, 0 );
   }

	return true;
}

int Fader::queryTouchValue( const int fileDescriptor ){

	memset( m_cRx, 0, sizeof(m_cRx) );
	m_sSpiADCTransferStruct.tx_buf = (unsigned long)m_cTouchTx ;
	m_sSpiADCTransferStruct.rx_buf = (unsigned long)m_cRx ;

   unsigned int  touchVal = queryDevice( fileDescriptor );
   bool bTouch = touchVal > 2900;

   if( bTouch != m_bTouch || m_bStart ){
      m_bTouch = bTouch;
      min = 6000;
      max = 0;
      if( m_pWindow )
      {
         char str[80];
         sprintf( str, "T %5s %5d", bTouch ? "true" : "false", touchVal );
         mvwaddstr( m_pWindow, 10 ,2 , str  );
         wrefresh( m_pWindow );
      }
   }

	return true;
}

int Fader:: queryDevice( const int fileDescriptor )
{
   int ret = ioctl( fileDescriptor, SPI_IOC_MESSAGE(1), &m_sSpiADCTransferStruct );
   if( ret == -1 )
   {
      close( fileDescriptor ) ;
      fprintf( stderr, "can't transfer data\n" ) ;
      return -1;
   }
   return (( m_cRx[1] & 0x0F ) << 8 ) | m_cRx[2];
}

float Fader::getDeviceFaderValue( int currentValue  )
{
   return static_cast<float>( currentValue ) / m_iInternalFaderRange * m_oDeviceFaderConfig.m_fValueRange + m_oDeviceFaderConfig.m_fMinValue;
}

int Fader::getInternalFaderValue( float deviceFaderValue )
{
   return (deviceFaderValue - m_oDeviceFaderConfig.m_fMinValue ) / m_oDeviceFaderConfig.m_fValueRange *  m_iInternalFaderRange;
}

void Fader::sendDeviceFaderValue()
{
   // only send value to the device when the fader ist touched
   if( m_bTouch  && m_pChannel )
   {
      float deviceFaderValue = getDeviceFaderValue( m_iCurrentFaderValue );
      m_pChannel->sendOscFaderValue( deviceFaderValue );
   }
}

} // namespace mofa