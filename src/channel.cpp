#include "channel.h"

#include <cstring>
#include <sys/ioctl.h>
#include <unistd.h>

#include "device.h"

namespace mofa
{

Channel::Channel( Device *device, int channelNumber, unsigned char spiCsADCPort, unsigned char spiCsGPIOPort ) 
    : m_pDevice(device), 
    m_iChannelNumber( channelNumber ), 
    m_cSpiCsADCPort( spiCsADCPort ),
    m_cSpiCsGPIOPort( spiCsGPIOPort )
{
    if( !m_pDevice )
        return;
    // store two structs for spi communication
    // all adc queries
    m_sSpiADCTransferStruct = device->getSpiTransferData();
    m_sSpiADCTransferStruct.cs_change = spiCsADCPort;
    m_sSpiADCTransferStruct.len = 3;
    m_sSpiADCTransferStruct.delay_usecs   = 100;
    m_sSpiADCTransferStruct.bits_per_word = 8;
    m_sSpiADCTransferStruct.speed_hz      = 1000000;

    //all gpio queries
    m_sSpiGPIOTransferStruct = device->getSpiTransferData();
    m_sSpiGPIOTransferStruct.cs_change = spiCsGPIOPort;
    m_sSpiGPIOTransferStruct.bits_per_word = 8;

    m_pFader = std::make_shared<Fader>( this );
    m_pMuteButton = std::make_shared<Button>( this, "mute", static_cast<unsigned int>(2), static_cast<unsigned int>(16) );
    m_pSoloButton = std::make_shared<Button>( this, "solo", static_cast<unsigned int>(3), static_cast<unsigned int>(17) );
}

void Channel::setWindow( WINDOW *window )
{
    m_pWindow = window;


    if( m_pFader )
        m_pFader->setWindow( window );
    if( m_pMuteButton )
        m_pMuteButton->setWindow( window );
    if( m_pSoloButton )
        m_pSoloButton->setWindow( window );
}


bool Channel::poll( int deviceNumber )
{
    if( m_pFader )
    {
        if( ! m_pFader->queryFaderValue( deviceNumber ) )
            return false;
        if( !m_pFader->queryTouchValue( deviceNumber ) )
            return false;
    }
    if( m_pMuteButton )
        if( !m_pMuteButton->queryButtonValue( deviceNumber ) )
            return false;
    if( m_pSoloButton )
        if( !m_pSoloButton->queryButtonValue( deviceNumber ) )
            return false;
    
    return true;
}

void Channel::sendOscFaderValue( float oscDeviceFaderValue )
{
    if( m_pDevice && m_iChannelNumber )
    {
        m_pDevice->getOscClient()->send( "/strip/fader", "if" , m_iChannelNumber, oscDeviceFaderValue );
        if( m_pWindow )
            mvwaddstr( m_pWindow, 3 ,1 , " /strip/fader" );
    }
}

void Channel::sendOscChannelButtonState( std::string button, bool state )
{
    if( m_pDevice && m_iChannelNumber )
    {
        m_pDevice->getOscClient()->send( "/strip/" + button, "ii" , m_iChannelNumber, state?1:0 );
        if( m_pWindow )
            mvwprintw( m_pWindow, 3 ,1 , " /strip/%s", button );
    }
}

spiTransferType Channel::getAdcSpiTransferData()
{
    return m_sSpiADCTransferStruct;
}

spiTransferType Channel::getGpioSpiTransferData()
{
    return m_sSpiGPIOTransferStruct;
}

bool Channel::getAdcTxBytes( int single, int adcPort, unsigned char txBytes[3] ){
    if( single > 1 || adcPort > 7 )
        return false;

    // we need the strucuure [startbit][single/differential bit][portbit 1][portbit 2][portbit 3]
    unsigned char data = 0;
    data |= 16;  //startbit
    data |= single *8; // single or differntial
    data |= adcPort;

    txBytes[0] = 0;
    // we need 5 zero bits in the first byte to comply with chip specification
    txBytes[1] = (data & 3) << 6; // store the first two bits in byte 1 and mve them up 6 bits
    txBytes[0] = data >> 2; // move the first two bits out of byte 0
    txBytes[2] = 0; // placeholder for return value

    return true;
}

int Channel::queryAdcDevice( const int fileDescriptor, spiTransferType *transferData ){
    char cxBytes[3];
	memset( &cxBytes, 0, sizeof(cxBytes) );
	transferData->rx_buf = (unsigned long)cxBytes ;

    int ret = ioctl( fileDescriptor, SPI_IOC_MESSAGE(1), transferData );
    if( ret == -1 )
    {
        close( fileDescriptor ) ;
        fprintf( stderr, "can't transfer data\n" ) ;
        return -1;
    }
    return (( cxBytes[1] & 0x0F ) << 8 ) | cxBytes[2];
}


} // namespace mofa

