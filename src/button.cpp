#include "button.h"

#include <pigpio.h> 
#include <unistd.h>

#include "channel.h"

namespace mofa
{


Button::Button( Channel *channel, std::string button, unsigned int adcPort, unsigned int gpioPort  ) : 
    m_pChannel( channel ), m_sButton( button), m_iAdcPort( adcPort ), m_iGpioPort( gpioPort ) 
{
   m_sSpiADCTransferStruct = m_pChannel->getAdcSpiTransferData();
   m_sSpiGPIOTransferStruct = m_pChannel->getGpioSpiTransferData();

   if( !channel->getAdcTxBytes( 1, m_iAdcPort, m_cTx ) )
      return;

   gpioSetMode( m_iGpioPort, PI_OUTPUT );
   gpioWrite( m_iGpioPort, 0 );

}


int Button::queryButtonValue( const int fileDescriptor ){

	m_sSpiADCTransferStruct.tx_buf = (unsigned long)m_cTx ;
    int  val = m_pChannel->queryAdcDevice( fileDescriptor, &m_sSpiADCTransferStruct );

    if( val == -1 )
        return false;

    if( val > 4000  )
        m_bHigh = true;
    else if ( m_bHigh && ( val < 4000 )  )
    {
        usleep( 100 );
        m_pChannel->sendOscChannelButtonState( m_sButton, !m_bState );
        //m_bSetPointState = !m_bState;
        m_bHigh = false;
    }

    if(  m_bSetPointState != m_bState )
    {
        m_bState = m_bSetPointState;

        if( m_bState )
            gpioWrite( m_iGpioPort, 1 );
        else
            gpioWrite( m_iGpioPort, 0 );

        if( m_pWindow )
        {
            char str[80];
            sprintf( str, "M       %5d %5s", val, m_bState ? "True" : "False" );
            mvwaddstr( m_pWindow, 11 ,2 , str  );
            wrefresh( m_pWindow );
        }

    }

    return true;
}

void Button::setInternalButtonState( bool state )
{
    m_bSetPointState = state;
}

} // namespace mofa