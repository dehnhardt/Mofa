#include "device.h"

#include <algorithm>
#include <cstring>
#include <fcntl.h>
#include <sys/ioctl.h>
#include <unistd.h>


namespace mofa
{

Device::Device()
{
    memset( &m_sSpiTransferData, 0, sizeof( spiTransferType ) );
    setDefaultSpiSettings();
}

void Device::setDefaultSpiSettings()
{
    spiDeviceConfig.speed  = 1000000;
    spiDeviceConfig.mode   = SPI_MODE_0;
    spiDeviceConfig.bits   = 8;
}

void Device::addChannel( unsigned char spiCsADCPort, unsigned char spiCsGPIOPort  )
{
    ++m_iNumChannels;
    ChannelPtr channel = std::make_shared<Channel>( this, m_iNumChannels, spiCsADCPort, spiCsGPIOPort );
    if( m_pWindow )
        channel->setWindow( m_pWindow );
    m_vChannels.push_back( std::move( channel ) );
}

ChannelPtr Device::getChannel( int channelNumber )
{
    if( channelNumber < 1 || channelNumber > m_iNumChannels )
        return nullptr;
    return m_vChannels[channelNumber -1];
}

bool Device::openSpiDevice( std::string devicePath )
{
    m_iSpiDeviceNumber = open( devicePath.c_str(), O_RDWR );
    if( m_iSpiDeviceNumber < 0 )
    {
        fprintf( stderr, "can't open device\n" ) ;
        return false;
    }
    m_sSpiDevicePath = devicePath;

    int ret = ioctl( m_iSpiDeviceNumber, SPI_IOC_WR_MODE, &spiDeviceConfig.mode );
    if( ret == -1 )
    {
        close( m_iSpiDeviceNumber ) ;
        fprintf( stderr, "can't set mode\n" ) ;
        return false;
    }

    ret = ioctl( m_iSpiDeviceNumber, SPI_IOC_WR_BITS_PER_WORD, &spiDeviceConfig.bits ) ;
    if( ret == -1 )
    {
        close( m_iSpiDeviceNumber ) ;
        fprintf( stderr, "can't set bits\n" ) ;
        return false;
    }

    ret = ioctl( m_iSpiDeviceNumber, SPI_IOC_WR_MAX_SPEED_HZ, &spiDeviceConfig.speed ) ;
    if( ret == -1 )
    {
        close( m_iSpiDeviceNumber ) ;
        fprintf( stderr, "can't set speed\n" ) ;
        return false;
    }
    return true;
}

bool Device::createOscClient()
{
    m_pOscDevice = std::make_shared<lo::Address>( oscDeviceConfig.host, oscDeviceConfig.port );
    return m_pOscDevice ? true : false;
}

void Device::setupOscClient()
{
    if( m_pOscDevice )
        m_pOscDevice->send("/set_surface", "iiiiiiiii", 5, 47, 3, 3, 0, 0, 9000, 0, 0 );
}

void Device::closeSpiDevice()
{
    close( m_iSpiDeviceNumber );
}

bool Device::poll()
{
    for( ChannelPtr channel : m_vChannels )
    {
        if( !channel->poll( m_iSpiDeviceNumber ) )
        {
            return false;
        }
    }
    return true;
}

spiTransferType Device::getSpiTransferData()
{
    return m_sSpiTransferData;
}

/*************
 * callbacks *
**************/

void Device::setFaderValue( int channelNumber, float oscDeviceValue )
{
    ChannelPtr channel = getChannel( channelNumber );
    if( channel )
        channel->setInternalFaderValue( oscDeviceValue );
}

void Device::setMuteButtonState( int channelNumber, bool oscMuteButtonState )
{
    ChannelPtr channel = getChannel( channelNumber );
    if( channel )
        channel->setInternalMuteButtonState( oscMuteButtonState );

}

void Device::setSoloButtonState( int channelNumber, bool oscMuteButtonState )
{
    ChannelPtr channel = getChannel( channelNumber );
    if( channel )
        channel->setInternalSoloButtonState( oscMuteButtonState );

}

} //namespace mofa