#pragma once

#include <curses.h>
#include <linux/spi/spidev.h>
#include <lo/lo.h>
#include <lo/lo_cpp.h>
#include <memory>
#include <stdint.h>
#include <string>
#include <vector>

#include "channel.h"
#include "mofa_types.h"


namespace mofa
{

using OscDevicePtr = std::shared_ptr<lo::Address>;

class Device
{
    public:
        Device();
        ~Device() = default;

    public:  // methods

        /**
         * @brief Set the Osc Clients Host
         * 
         * @param host hostname or ip address
         */
        void setOscClientHost( std::string host )
        {
            oscDeviceConfig.host = host;
        }

        /**
         * @brief Set the Osc Clients Port
         * 
         * @param port portnumber as string
         */
        void setOscClientPort( std::string port )
        {
            oscDeviceConfig.port = port;
        }

        /**
         * @brief Create a Ocs Client object
         * 
         * @return true if the client is created successfully, false otherwise
         */
        bool createOscClient();

        /**
         * @brief setup the osc client
         * 
         */
        void setupOscClient();

        /**
         * @brief return the osc client
         * 
         * @return osc client if created, nullptr otherwise
         */
        OscDevicePtr getOscClient()
        {
            return m_pOscDevice;
        }

        /**
         * @brief open the hardware device
         * 
         * @return true if device can be opened
         */
        bool openSpiDevice( std::string devicePath );

        /**
         * @brief close the hardware device
         * 
         */
        void closeSpiDevice();

        /**
         * @brief add a channel
         * 
         * @param numChannels number of chanels to add
         */
        void addChannel( unsigned char spiCsADCPort, unsigned char spiCsGPIOPort );

        /**
         * @brief Get a channel object at poistion 'channelNumber'
         * 
         * @param channelNumber number of channel to retrieve
         * @return ChannelPtr 
         */
        ChannelPtr getChannel( int channelNumber );

        /**
         * @brief poll all controls
         * 
         * @return true if poll succeeds
         */
        bool poll();

        /**
         * @brief Set the a ncurses window object for outputs
         * 
         * @param window 
         */
        void setWindow( WINDOW *window ){
            m_pWindow = window;
        }
        
        /**
         * @brief Get the default spi transfer data object
         * 
         * @return spiTransferType 
         */
        spiTransferType getSpiTransferData();

        /************
        * Callbacks *
        ************/

        /**
         * @brief Set the fader value for a channel
         * 
         * @param channelNumber the channel for which the fader is to be set
         * @param oscDeviceValue the value, the fader should be set to
         */
        void setFaderValue( int channelNumber, float oscDeviceValue );

        /**
         * @brief Set the Mute Button State object
         * 
         * @param channelNumber the channel for which the fader is to be set
         * @param oscMuteButtonState the mute state from osc device 
         */
        void setMuteButtonState( int channelNumber, bool oscMuteButtonState );

        /**
         * @brief Set the Mute Button State object
         * 
         * @param channelNumber the channel for which the fader is to be set
         * @param oscSoloButtonState the mute state from osc device 
         */
        void setSoloButtonState( int channelNumber, bool oscSoloButtonState );

    private: //member variables

        /**
         * @brief the ncurses window for outputs
         */
        WINDOW                  *m_pWindow;

        /**
         * @brief osc client to communicate with
         */
        OscDevicePtr            m_pOscDevice = nullptr;

        /**
         * @brief count of configured channels
         */
        int                     m_iNumChannels = 0;

        /**
         * @brief array of all configurred channels
         */
        std::vector<ChannelPtr> m_vChannels;

        /**
         * @brief hardware path to the spi device
         */
        std::string             m_sSpiDevicePath;

        /**
         * @brief file descriptor id of the spi device
         */
        int                     m_iSpiDeviceNumber = -1;

        /**
         * @brief dafault struct for spi transfers
         */
      	spiTransferType         m_sSpiTransferData;
        
        std::vector<unsigned char> m_vSpiCsPort;

        struct SpiDeviceConfig {
            uint32_t    speed  = 1000000;
            uint8_t     mode   = SPI_MODE_0;
            uint8_t     bits   = 8;
        } spiDeviceConfig;

        struct OscDeviceConfig {
            std::string    host = "localhost";
            std::string    port = "9000";
        } oscDeviceConfig;


    private: // methods
        /**
         * @brief Set the default spi settings for the whol device
         * 
         * This settings can be edited in every child of Device
         * 
         */
        void setDefaultSpiSettings();

            

};

    using DevicePtr = std::shared_ptr<Device>;

} //namespace mofa