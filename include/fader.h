#pragma once

#include <atomic>
#include <curses.h>
#include <memory>

#include "mofa_types.h"

namespace mofa
{

class Channel;

class DeviceFaderConfig
{
	public:

		DeviceFaderConfig() = default;

		DeviceFaderConfig( float minValue, float maxValue ) : m_fMinValue( minValue ), m_fMaxValue( maxValue )
		{	
			m_fValueRange = m_fMaxValue - m_fMinValue;
		}

		float m_fMinValue 	= 0.0;
		float m_fMaxValue 	= 1.0;
		float m_fValueRange = 1.0;
};

class Fader
{
	public:
		/**
		 * @brief Construct a new Fader object
		 *
		 */
	  	Fader( Channel *channel );

		/**
		 * @brief Destroy the Fader object
		 *
		 */
		~Fader() = default;

		/**
		 * @brief query the value of the fader
		 *
		 * @param fileDescriptor descriptor of the spi device
		 * @return int returns the error code or zero
		 */
		int queryFaderValue( const int fileDescriptor );

		/**
		 * @brief query the value of thefader touch
		 *
		 * @param fileDescriptor descriptor of the spi device
		 * @return int returns the error code or zero
		 */
		int queryTouchValue( const int fileDescriptor );

        /**
         * @brief Set the a ncurses window object for outputs
         * 
         * @param window 
         */
        void setWindow( WINDOW *window ){
            m_pWindow = window;
        }

		/**
		 * @brief Set the Fader value from extern
		 *
		 * @param val
		 */
		void setInternalFaderValue( float oscDeviceValue );


	private:

        /**
         * @brief the ncurses window for outputs
         */
        WINDOW                  *m_pWindow;

		/**
		 * @brief pointer to the parent channel
		 * 
		 */
		Channel 				*m_pChannel;
		
		/**
		 * @brief holds the devices value range for faders
		 * 
		 */
		DeviceFaderConfig		m_oDeviceFaderConfig;

		/**
		 * @brief the internal value range for the fader
		 * 
		 */
		int 					m_iInternalFaderRange = 4095;

		/**
		 * @brief used to ensure to query the first value
		 */
		bool 					m_bStart = true;
	
		/**
		 * @brief the data receive buffer
		 */
		unsigned char			m_cRx[3];

		/**
		 * @brief the trasnmit values buffer for the fader value
		 */
		unsigned char    		m_cFaderTx[3];
		
		/**
		 * @brief the trasnmit values buffer for the touch value
		 */
		unsigned char 			m_cTouchTx[3];

		/**
		 * @brief the current fader value
		 */
	  	std::atomic<unsigned int>	m_iCurrentFaderValue;
		/**
		 * @brief the last stored fader value
		 */
	  	unsigned int 			m_iLastFaderValue = 0;
		/**
		 * @brief the value which is set from external
		 */
		unsigned int 			m_iFaderSetPointValue = 0;

		/**
		 * @brief true if the touch sense signals a fader touch
		 */
		bool 					m_bTouch = false;

   		/**
		 * @brief the structure which is sent to the spi-device for adc queries
		 */
      	spiTransferType m_sSpiADCTransferStruct;

   		/**
		 * @brief the structure which is sent to the spi-device for gpio queries
		 */
      	spiTransferType m_sSpiGPIOTransferStruct;
		
		/**
		 * @brief queries a value from the spi device
		 * 
		 * @param fileDescriptor 
		 * @return int 
		 */
		int queryDevice( const int fileDescriptor );

		/**
		 * @brief Set the config object for the devices fader range
		 * 
		 * @param deviceFaderConfig 
		 */
		void setDeviceFaderConfig( DeviceFaderConfig deviceFaderConfig ){
			m_oDeviceFaderConfig = deviceFaderConfig;
		}
		
		/**
		 * @brief converts the value from internal range to the devices fader range
		 * 
		 * @param currentValue internal value
		 * @return float devices value
		 */
		float getDeviceFaderValue( int currentValue );

		/**
		 * @brief converts the device fader value to an intern fader value
		 * 
		 * @param deviceFaderValue 
		 * @return int 
		 */
		int getInternalFaderValue( float deviceFaderValue );

		/**
		 * @brief sends the fader value to the device
		 * 
		 * @param currentValue the internal fader value to be sent
		 */
		void sendDeviceFaderValue();

};

using FaderPtr = std::shared_ptr<Fader>;

} // namespace mofa
