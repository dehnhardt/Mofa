#pragma once

#include <curses.h>
#include <memory>

#include "mofa_types.h"

namespace mofa
{

class Channel;

class Button
{
    public:
        Button( Channel *channel, std::string button, unsigned int adcPort, unsigned int gpioPort );

        ~Button() = default;

		/**
		 * @brief query the value of the button
		 *
		 * @param fileDescriptor descriptor of the spi device
		 * @return int returns the error code or zero
		 */
		int queryButtonValue( const int fileDescriptor );
        
        /**
         * @brief set the internal state of the button
         * 
         * @param state state to set
         */
        void setInternalButtonState( bool state );

        /**
         * @brief Set the a ncurses window object for outputs
         * 
         * @param window 
         */
        void setWindow( WINDOW *window ){
            m_pWindow = window;
        }


    private:
        /**
         * @brief holds the current state of the current query
         */
        bool m_bState = false;

        /**
         * @brief holds the set point state of the current query
         */
        bool m_bSetPointState = false;

        /**
         * @brief only set, when the button is pressed
         */
        bool m_bHigh = false;

        /**
         * @brief the ncurses window for outputs
         */
        WINDOW                  *m_pWindow;

		/**
		 * @brief pointer to the parent channel
		 * 
		 */
		Channel 				*m_pChannel;

        /**
         * @brief the name of the button which is send to the osc device
         * 
         */
        std::string             m_sButton;

        /**
         * @brief the port on the adc chip
         */
        unsigned int            m_iAdcPort = 0;

        /**
         * @brief the port on the gpio chip
         */
        unsigned int            m_iGpioPort = 0;

		/**
		 * @brief the trasnmit values buffer for the fader value
		 */
		unsigned char    		m_cTx[3];


   		/**
		 * @brief the structure which is sent to the spi-device for adc queries
		 */
      	spiTransferType m_sSpiADCTransferStruct;

   		/**
		 * @brief the structure which is sent to the spi-device for gpio queries
		 */
      	spiTransferType m_sSpiGPIOTransferStruct;


};

using ButtonPtr = std::shared_ptr<Button>;

}
