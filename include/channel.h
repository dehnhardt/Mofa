#pragma once

#include <curses.h>
#include <memory>

#include "button.h"
#include "fader.h"
#include "mofa_types.h"

namespace mofa
{

class Device;

class Channel
{
    public:
        Channel( Device *device, int channelNumber, unsigned char spiCsADCPort, unsigned char spiCsGPIOPort );
        ~Channel() = default;

    public: //methods

        /**
         * @brief Set the a ncurses window object for outputs
         * 
         * @param window 
         */
        void setWindow( WINDOW *window );

        /**
         * @brief polls all controls of this channel
         * 
         * @param spiDeviceNumber file descriptor of the spi device
         * @return true if all polls are sucessfull
         */
        bool poll( int spiDeviceNumber );



        /**
         * @brief sends the fader value to the oscDevice
         * 
         * @param faderValue the value to send in osc devices value range
         */
        void sendOscFaderValue( float oscDeviceFaderValue );

   		/**
		 * @brief Set the fader value from extern
		 *
		 * @param val
		 */
		void setInternalFaderValue( float oscDeviceValue )
        {
            if( m_pFader )
                m_pFader->setInternalFaderValue( oscDeviceValue );
        }
        
        /**
         * @brief sends the button state to the oscDevice
         * 
         * @param state 
         */
        void sendOscChannelButtonState( std::string button, bool state );

        /**
         * @brief set the  mute button state from extern
         * 
         * @param state 
         */
        void setInternalMuteButtonState( float state )
        {
            if( m_pMuteButton )
                m_pMuteButton->setInternalButtonState( state == 1.0 );
        }

        /**
         * @brief set the solo button state from extern
         * 
         * @param state 
         */
        void setInternalSoloButtonState( float state )
        {
            if( m_pSoloButton )
                m_pSoloButton->setInternalButtonState( state == 1.0 );
        }


        /**
         * @brief Get the transfer bytes for an adc port
         * 
         * @param single should the sample should be a single port or differential of two ports
         * @param adcPort the port of a single sample or the lower port of a differential sample
         * @param txBytes the three bytes to be filled
         * @return true if the creation of the tx bytes succeeds, false otherwise
         */
        bool getAdcTxBytes( int single, int adcPort, unsigned char txBytes[3] );

        /**
         * @brief Get the default spi transfer data object for the adc
         * 
         * @return spiTransferType 
         */
        spiTransferType getAdcSpiTransferData();

        /**
         * @brief Get the default spi transfer data object for the adc
         * 
         * @return spiTransferType 
         */
        spiTransferType getGpioSpiTransferData();

   		/**
		 * @brief queries a value from the adc spi device
		 * 
		 * @param fileDescriptor 
         * @param transferData 
		 * @return int -1 if something went wrong, the value of the adc conversion otherwise
		 */
		int queryAdcDevice( const int fileDescriptor, spiTransferType *transferData );

    private: // members

        /**
         * @brief pointer to the device
         */
        Device *        m_pDevice = nullptr;

        /**
         * @brief number of the channel (1-based index)
         */
        int             m_iChannelNumber = 0;

        /**
         * @brief the gpio port with which the adc is selectable
         */
        unsigned char   m_cSpiCsADCPort = 0;

        /**
         * @brief  the gpio port with which the gpio extension is selectable
         */
        unsigned char   m_cSpiCsGPIOPort = 0;

   		/**
		 * @brief the structure which is sent to the spi-device for adc queries
		 */
      	spiTransferType m_sSpiADCTransferStruct;

   		/**
		 * @brief the structure which is sent to the spi-device for gpio queries
		 */
      	spiTransferType m_sSpiGPIOTransferStruct;


        /**
         * @brief pointer to the fader
         */
        FaderPtr        m_pFader = nullptr;

        /**
         * @brief pointer to the mute button
         */
        ButtonPtr       m_pMuteButton = nullptr;

        /**
         * @brief pointer to the solo button
         */
        ButtonPtr       m_pSoloButton = nullptr;

        /**
         * @brief pointer to an ncurses output window
         */
        WINDOW          *m_pWindow = nullptr;

};

using ChannelPtr = std::shared_ptr<Channel>;

} // namespae mofa

